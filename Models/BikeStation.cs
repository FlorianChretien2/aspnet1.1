using System;

namespace MvcMovie.Models
{
    public class BikeStation
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public int BikeCount { get; set; }

        public int SlotCount { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public bool IsAvailable { get; set; }
    }
}