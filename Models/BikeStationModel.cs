using System;
using System.Collections.Generic;

namespace MvcMovie.Models
{
    public class BikeStationModel
    {
        public List<BikeStation> StationList { get; set; }

        public BikeStationModel()
        {
            var rd = new Random();
            StationList = new List<BikeStation>
            {
                new BikeStation 
                {
                    Id = 46,
                    Name = "Berges de Garonne",
                    BikeCount = rd.Next(0, 20),
                    SlotCount = rd.Next(0, 20),
                    IsAvailable = rd.Next(0, 5) < 3,
                    Latitude = "44.822803",
                    Longitude = "-0.55293"
                },
                new BikeStation 
                {
                    Id = 159,
                    Name = "CAPC",
                    BikeCount = rd.Next(0, 20),
                    SlotCount = rd.Next(0, 20),
                    IsAvailable = true,
                    Latitude = "44.84978",
                    Longitude = "-0.57024"
                },
                new BikeStation 
                {
                    Id = 59,
                    Name = "Capucins",
                    BikeCount = rd.Next(0, 20),
                    SlotCount = rd.Next(0, 20),
                    IsAvailable = true,
                    Latitude = "44.82995",
                    Longitude = "-0.56812"
                },
                new BikeStation 
                {
                    Id = 166,
                    Name = "Centre commercial du Lac",
                    BikeCount = rd.Next(0, 20),
                    SlotCount = rd.Next(0, 20),
                    IsAvailable = true,
                    Latitude = "44.88192",
                    Longitude = "-0.565221"
                },
                new BikeStation 
                {
                    Id = 115,
                    Name = "Darwin",
                    BikeCount = rd.Next(0, 20),
                    SlotCount = rd.Next(0, 20),
                    IsAvailable = false,
                    Latitude = "44.84932",
                    Longitude = "-0.56217"
                },
                new BikeStation 
                {
                    Id = 58,
                    Name = "Chartrons",
                    BikeCount = rd.Next(0, 20),
                    SlotCount = rd.Next(0, 20),
                    IsAvailable = true,
                    Latitude = "44.85321",
                    Longitude = "-0.56727"
                },
                new BikeStation 
                {
                    Id = 32,
                    Name = "Parc Bordelais",
                    BikeCount = rd.Next(0, 20),
                    SlotCount = rd.Next(0, 20),
                    IsAvailable = true,
                    Latitude = "44.89391",
                    Longitude = "-0.566225"
                },
                new BikeStation 
                {
                    Id = 106,
                    Name = "Place de la Victoire",
                    BikeCount = rd.Next(0, 20),
                    SlotCount = rd.Next(0, 20),
                    IsAvailable = rd.Next(0, 5) < 2,
                    Latitude = "44.83064",
                    Longitude = "-0.57321"
                }
            };
        }
    }
}